
package com.dealfish.app.lesson4.complete.droid;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import android.app.Activity;

@RunWith(RobolectricTestRunner.class)
public class AdvancedCategoryActivityTest {

	@Test
	public void testSomething() throws Exception {
	    Activity activity = Robolectric.buildActivity(AdvancedCategoryActivity.class).create().get();
	    Assert.assertTrue(activity != null);
	  }
}
