package com.dealfish.app;

import javax.inject.Singleton;

import sprint3r.deans4j.droid.catalogue.gateways.DFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.gateways.DFProductGateway;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.gateways.stubs.ModelHolders;
import sprint3r.deans4j.droid.catalogue.gateways.stubs.StubCallbackDFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.gateways.stubs.StubCallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.gateways.stubs.StubDFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.gateways.stubs.StubDFProductGateway;

import com.dealfish.app.modules.AppModule;

import dagger.Module;
import dagger.Provides;

@Module(library = true, includes = AppModule.class, complete = false, overrides=true)
public class MockGatewayModule {
	
	@Provides @Singleton
	public ModelHolders modelHolders() {
		return new ModelHolders();
	}

	@Provides @Singleton
	public DFProductGateway normalProductGateway(StubDFProductGateway stub) {
		return stub;
	}
	
	@Provides @Singleton
	public CallbackDFProductGateway callbackProductGateway(StubCallbackDFProductGateway stub) {
		return stub;
	}
	
	@Provides @Singleton
	public DFCategoryGateway normalCategoryGateway(StubDFCategoryGateway stub) {
		return stub;
	}
	
	@Provides @Singleton
	public CallbackDFCategoryGateway callbackCategoryGateway(StubCallbackDFCategoryGateway stub) {
		return stub;
	}
	
}
