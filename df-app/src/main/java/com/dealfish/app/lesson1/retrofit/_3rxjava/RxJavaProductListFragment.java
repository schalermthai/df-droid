package com.dealfish.app.lesson1.retrofit._3rxjava;

import javax.inject.Inject;

import rx.Observer;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.android.schedulers.AndroidSchedulers;
import sprint3r.deans4j.droid.catalogue.gateways.observable.ObservableDFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.dealfish.app.droid.DFBaseListFragment;

public class RxJavaProductListFragment extends DFBaseListFragment implements Observer<SearchResult<Product>> {

	@Inject
	ObservableDFProductGateway gateway;
	
	private ArrayAdapter<String> mAdapter;

	private Subscription subscribe;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		mAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1);

		setEmptyText("No products");
		setListAdapter(mAdapter);
		setListShown(false);
		
//		gateway.listProductByCategory(1, 0, 100).subscribe(this);
		subscribe = AndroidObservable.fromFragment(this, gateway.listProductByCategory(1, 0, 100))
	        .observeOn(AndroidSchedulers.mainThread())
	        .subscribe(this);
	}

	public void onNext(SearchResult<Product> data) {
		mAdapter.clear();

		if (data != null && data.getItems() != null) {
			for (Product d : data.getItems()) {
				mAdapter.add(d.getName());
			}
		}

		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
	}
	
	public void onCompleted() { }

	public void onError(Throwable e) {
		mAdapter.clear();
		setListShown(true);
		Toast.makeText(getActivity(), "something bad happen", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		subscribe.unsubscribe();
	}
}