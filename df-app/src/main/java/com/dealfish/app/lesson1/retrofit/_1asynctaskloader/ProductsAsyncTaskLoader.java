package com.dealfish.app.lesson1.retrofit._1asynctaskloader;

import javax.inject.Inject;

import sprint3r.deans4j.droid.catalogue.gateways.DFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;

import com.dealfish.app.droid.ForDFApplication;

public class ProductsAsyncTaskLoader extends AsyncTaskLoader<SearchResult<Product>> {

	private SearchResult<Product> mData;

	private DFProductGateway gateway;

	private int categoryId = 1;

	@Inject
	public ProductsAsyncTaskLoader(@ForDFApplication Context ctx, DFProductGateway gateway) {
		// Loaders may be used across multiple Activitys (assuming they aren't
		// bound to the LoaderManager), so NEVER hold a reference to the context
		// directly. Doing so will cause you to leak an entire Activity's
		// context.
		// The superclass constructor will store a reference to the Application
		// Context instead, and can be retrieved with a call to getContext().
		super(ctx);
		this.gateway = gateway;
	}

	@Override
	public SearchResult<Product> loadInBackground() {
		return gateway.listProductByCategory(categoryId, 0, 100);
	}
	
	@Override
	protected void onStartLoading() {
		if (mData != null) {
			deliverResult(mData);
		}

		if (takeContentChanged() || mData == null) {
			forceLoad();
		}
	}

	public Loader<SearchResult<Product>> categoryId(int id) {
		this.categoryId = id;
		return this;
	}

}