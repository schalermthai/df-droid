package com.dealfish.app.lesson1.retrofit._1asynctaskloader;

import javax.inject.Inject;

import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.dealfish.app.droid.DFBaseListFragment;

public class LoaderProductListFragment extends DFBaseListFragment implements LoaderManager.LoaderCallbacks<SearchResult<Product>> {

	@Inject
	ProductsAsyncTaskLoader sampleLoader;

	private ArrayAdapter<String> mAdapter;


	private static final int LOADER_ID = 1;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		mAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1);

		setEmptyText("No products");
		setListAdapter(mAdapter);
		setListShown(false);

		getLoaderManager().initLoader(LOADER_ID, null, this);
	}
	
	public Loader<SearchResult<Product>> onCreateLoader(int id, Bundle args) {
		return sampleLoader.categoryId(1);
	}

	public void onLoadFinished(Loader<SearchResult<Product>> loader,
			SearchResult<Product> data) {

		mAdapter.clear();

		if (data != null && data.getItems() != null) {
			for (Product d : data.getItems()) {
				mAdapter.add(d.getName());
			}
		}

		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
	}

	public void onLoaderReset(Loader<SearchResult<Product>> loader) {
		mAdapter.clear();
		setListShown(true);
		Toast.makeText(getActivity(), "something bad happen", Toast.LENGTH_SHORT).show();
	}
}
