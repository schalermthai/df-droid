package com.dealfish.app.lesson3.tape.droid;

import javax.inject.Inject;

import sprint3r.deans4j.droid.catalogue.models.Category;
import sprint3r.deans4j.droid.catalogue.models.PostProduct;
import sprint3r.deans4j.droid.catalogue.models.Product;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.dealfish.app.R;
import com.dealfish.app.domains.loaders.CategoryLoader;
import com.dealfish.app.domains.tape.NewPostTaskItem;
import com.dealfish.app.domains.tape.NewPostTaskQueue;
import com.dealfish.app.droid.DFBaseActivity;
import com.dealfish.app.lesson4.complete.droid.AdvancedCategoryActivity;
import com.squareup.otto.Subscribe;

public class PostNewProductActivity extends DFBaseActivity implements OnItemSelectedListener {

	@Inject
	CategoryLoader loader;

	@Inject
	NewPostTaskQueue queue;
	
	private ArrayAdapter<String> categoriesAdapter;

	private Category[] categories;

	private EditText nameField;
	private EditText priceField;
	private EditText descriptionField;

	private Category selectedCategory;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post_new_product);

		setTitle("Create new post");
		
		categoriesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
		categoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		Spinner categoriesSpinner = (Spinner) findViewById(R.id.categorySpinner);
		categoriesSpinner.setAdapter(categoriesAdapter);
		categoriesSpinner.setOnItemSelectedListener(this);
		
		nameField = (EditText) findViewById(R.id.nameField);
		priceField = (EditText) findViewById(R.id.priceField);
		descriptionField = (EditText) findViewById(R.id.descriptionField);

		loader.requestListOfCategories();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.post, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_post:
			startService(new Intent(getApplication(), PostNewProductProcessorService.class));
			queue.add(new NewPostTaskItem(createPostProduct()));
			navigateToMainActiivty();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private PostProduct createPostProduct() {
		
		Product productToPost = new Product();
		productToPost.setName(nameField.getText().toString());
		productToPost.setPrice(parseDouble(priceField.getText().toString()));
		productToPost.setDescription(descriptionField.getText().toString());
		productToPost.setCategoryId(selectedCategory.getCategoryId());

		return new PostProduct("manee", productToPost);
	}

	private void navigateToMainActiivty() {
		Intent intent = new Intent(this, AdvancedCategoryActivity.class);
		startActivity(intent);
	}

	@Subscribe
	public void onCategoryLoaded(Category[] catagories) {
		this.categories = catagories;
		updateCategoryList();
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		selectedCategory = categories[position];
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	private void updateCategoryList() {
		categoriesAdapter.clear();
		for (Category cat : categories) {
			categoriesAdapter.add(cat.getName());
		}
	}
	
	private Double parseDouble(String text) {
		try{
		    return Double.parseDouble(text);
		} catch (final NumberFormatException e) {
		    return 0.0;
		}
	}
}


