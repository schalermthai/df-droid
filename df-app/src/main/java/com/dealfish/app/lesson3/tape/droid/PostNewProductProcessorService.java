package com.dealfish.app.lesson3.tape.droid;

import javax.inject.Inject;

import android.content.Intent;
import android.widget.Toast;

import com.dealfish.app.domains.tape.NewPostTaskQueue.PostNewProductQueueSizeEvent;
import com.dealfish.app.domains.tape.QueueProcessor.PostNewProductSuccessEvent;
import com.dealfish.app.domains.tape.QueueProcessor;
import com.dealfish.app.droid.DFBaseService;
import com.squareup.otto.Subscribe;

public class PostNewProductProcessorService extends DFBaseService {
	
	@Inject
	QueueProcessor processor;
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		processor.processNext();
		return START_STICKY;
	}
	
	@Subscribe
	public void on(PostNewProductQueueSizeEvent sizeChangeEvent) {
		if (sizeChangeEvent.getSize() == 0) {
		}
	}
	
	@Subscribe
	public void onUploadSuccess(PostNewProductSuccessEvent event) {
		 Toast.makeText(getApplicationContext(), "task completed for: " + event.product.getName(), Toast.LENGTH_LONG).show();
	}
}
