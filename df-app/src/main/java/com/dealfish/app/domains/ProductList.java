package com.dealfish.app.domains;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import sprint3r.deans4j.droid.catalogue.models.Category;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

import com.dealfish.app.domains.loaders.advanced.ProductPaginationLoaderFactory;
import com.dealfish.app.domains.loaders.advanced.SearchResultPaginationLoader;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class ProductList {

	private final Category corespondingCateogry;
	private final List<Product> products;
	private final SearchResultPaginationLoader<Product> productPaginationLoader;
	private final Bus bus;

	ProductList(Category selecCategory, SearchResultPaginationLoader<Product> productLoader, Bus bus) {
		this.corespondingCateogry = selecCategory;
		this.productPaginationLoader = productLoader;
		this.products = new ArrayList<Product>();
		this.bus = bus;
	}
	
	@Subscribe
	public void onDataLoaded(SearchResult<Product> searchResult) {
		products.addAll(Arrays.asList(searchResult.getItems()));
		bus.post(new OnDataChangedEvent(products));
	}
	
	public Category getCorespondingCategory() {
		return corespondingCateogry;
	}
	
	public List<Product> getProducts() {
		return products;
	}
	
	public boolean hasMore() {
		return productPaginationLoader.hasMore();
	}
	
	public void loadMore() {
		productPaginationLoader.loadNext();
	}
	
	public void cancelRecentLoading() {
		productPaginationLoader.cancelRecentLoading();
	}
	
	public static class OnDataChangedEvent {

		public final List<Product> products;
		
		public OnDataChangedEvent(List<Product> products) {
			this.products = products;
		}
	}
	
	public static class ProductListFactory {
		
		@Inject
		ProductPaginationLoaderFactory loaderFactory;
		
		@Inject
		Bus bus;
		
		ProductList recent;
		
		public ProductList getInstance(Category category) {
			return new ProductList(category, loaderFactory.loadByCategory(category.getCategoryId()), bus);
		}

	}
	
}
