package com.dealfish.app.domains.callbacks;

import javax.inject.Inject;
import javax.inject.Singleton;

import sprint3r.deans4j.droid.catalogue.models.Product;

import com.squareup.otto.Bus;
import com.squareup.otto.Produce;

@Singleton
public class OttoProductCache extends OttoCacheCallback<Product> {
	
	@Inject
	public OttoProductCache(Bus bus) {
		super(bus);
		bus.register(this);
	}
	
	@Override @Produce
	public Product[] getCachedValue() {
		return super.getCachedValue();
	}
}
