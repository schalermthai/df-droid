package com.dealfish.app.domains.tape;

import java.io.File;

import javax.inject.Named;
import javax.inject.Singleton;

import sprint3r.deans4j.droid.catalogue.modules.JacksonModule;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.otto.Bus;
import com.squareup.tape.InMemoryObjectQueue;

import dagger.Module;
import dagger.Provides;

@Module(library = true, includes = JacksonModule.class, complete = false)
public class TapeModule {

	@Provides @Singleton
	public InMemoryObjectQueue<NewPostTaskItem> task(@Named("queueFile") File queueFile, ObjectMapper objectMapper) {
		return new InMemoryObjectQueue<NewPostTaskItem>();
	}

	@Provides @Singleton
	public NewPostTaskQueue create(InMemoryObjectQueue<NewPostTaskItem> task, NewPostTaskItemDependencyInjector taskInjector, Bus bus) {
		return new NewPostTaskQueue(task, taskInjector, bus);
	}

}
