package com.dealfish.app.domains.loaders.advanced;

import retrofit.Callback;

public interface DoRequestHandler<T> {

	public void call(int offset, int limit, Callback<T> callback);
}
