package com.dealfish.app.domains;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import sprint3r.deans4j.droid.catalogue.models.Category;

import com.dealfish.app.domains.loaders.CategoryLoader;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

@Singleton
public class CategoryList {

	private List<Category> categories;
	private CategoryLoader categoryLoader;
	private Bus bus;
	
	@Inject
	public CategoryList(CategoryLoader categoryLoader, Bus bus) {
		this.categoryLoader = categoryLoader;
		this.categories = new ArrayList<Category>();
		
		this.bus = bus;
	}

	@Subscribe
	public void onLoaded(Category[] data) {
		this.categories = Arrays.asList(data);
		bus.post(new OnDataChangedEvent(categories));
	}
	
	public void loadAll() {
		categoryLoader.requestListOfCategories();
	}
	
	public List<Category> getCategories() {
		return categories;
	}
	
	public static class OnDataChangedEvent {

		public final List<Category> categories;

		public OnDataChangedEvent(List<Category> categories) {
			this.categories = categories;
		}
	}
	
}
