package com.dealfish.app.domains.callbacks;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.squareup.otto.Bus;
import com.squareup.otto.Produce;

import sprint3r.deans4j.droid.catalogue.models.Category;

@Singleton
public class OttoCategoryCache extends OttoCacheCallback<Category> {

	@Inject
	public OttoCategoryCache(Bus bus) {
		super(bus);
		bus.register(this);
	}
	
	@Override @Produce
	public Category[] getCachedValue() {
		return super.getCachedValue();
	}
}
