package com.dealfish.app.domains.loaders.advanced;

import retrofit.Callback;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

public class ProductByCategoryDoRequestHandler implements DoRequestHandler<SearchResult<Product>> {
	
	private final Integer categoryId;
	private final CallbackDFProductGateway gateway;
	
	public ProductByCategoryDoRequestHandler(Integer categoryId, CallbackDFProductGateway gateway) {
		this.categoryId = categoryId;
		this.gateway = gateway;
	}

	@Override
	public void call(int offset, int limit, Callback<SearchResult<Product>> callback) {
		gateway.listProductByCategory(categoryId, offset, limit, callback);
	}

}
