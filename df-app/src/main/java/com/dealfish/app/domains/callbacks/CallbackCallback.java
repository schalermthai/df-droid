package com.dealfish.app.domains.callbacks;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CallbackCallback<T> implements Callback<T> {
	
	Callback<T> proxy;
	Callback<T> delegate;
	
	public CallbackCallback(Callback<T> proxy, Callback<T> delegate) {
		this.proxy = proxy;
		this.delegate = delegate;
	}

	@Override
	public void success(T t, Response response) {
		delegate.success(t, response);
		proxy.success(t, response);
	}

	@Override
	public void failure(RetrofitError error) {
		delegate.failure(error);
		proxy.failure(error);
	}

}
