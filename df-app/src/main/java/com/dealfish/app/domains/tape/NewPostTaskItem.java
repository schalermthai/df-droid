package com.dealfish.app.domains.tape;

import retrofit.Callback;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.PostProduct;
import sprint3r.deans4j.droid.catalogue.models.Product;

import com.squareup.tape.Task;

public class NewPostTaskItem implements Task<Callback<Product>> {
	
	private static final long serialVersionUID = 385358359752146292L;

	private final PostProduct postProduct;
	
	private CallbackDFProductGateway gateway;
	
	public NewPostTaskItem(PostProduct p) {
		this.postProduct = p;
	}

	@Override
	public void execute(Callback<Product> callback) {
		gateway.post(postProduct, callback);
	}
	
	public PostProduct getPostProduct() {
		return postProduct;
	}
	
	public void setGateway(CallbackDFProductGateway gateway) {
		this.gateway = gateway;
	}

}
 