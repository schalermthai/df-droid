package com.dealfish.app.domains.loaders;

import javax.inject.Inject;
import javax.inject.Singleton;

import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

import com.dealfish.app.domains.callbacks.CancellableCallback;
import com.dealfish.app.domains.callbacks.OttoProductCache;

@Singleton
public class ProductsLoader {

	private CallbackDFProductGateway gateway;
	private OttoProductCache cachedCallback;

	@Inject
	public ProductsLoader(CallbackDFProductGateway gateway,
			OttoProductCache cachedCallback) {
		this.gateway = gateway;
		this.cachedCallback = cachedCallback;
	}

	public CancellableCallback<SearchResult<Product>> loadByCategory(Integer categoryId, Integer offset, Integer limit) {
		CancellableCallback<SearchResult<Product>> requestCall = new CancellableCallback<SearchResult<Product>>(cachedCallback);
		gateway.listProductByCategory(categoryId, offset, limit, requestCall);
		return requestCall;
	}

}
