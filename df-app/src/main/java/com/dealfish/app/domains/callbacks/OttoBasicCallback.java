package com.dealfish.app.domains.callbacks;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.dealfish.app.domains.SomethingBadHappendEvent;
import com.squareup.otto.Bus;

public class OttoBasicCallback<T> implements Callback<T> {

	private Bus bus;
	
	public OttoBasicCallback(Bus bus) {
		this.bus = bus;
	}

	@Override
	public void success(T result, Response response) {
		bus.post(result);
	}

	@Override
	public void failure(RetrofitError error) {
		bus.post(new SomethingBadHappendEvent(error));
	}

}
