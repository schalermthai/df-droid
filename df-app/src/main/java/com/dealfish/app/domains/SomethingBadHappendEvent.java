package com.dealfish.app.domains;

public class SomethingBadHappendEvent {
	private Throwable throwable;

	public SomethingBadHappendEvent(Throwable t) {
		this.throwable = t;
	}
	
	public Throwable getThrowable() {
		return throwable;
	}
}
