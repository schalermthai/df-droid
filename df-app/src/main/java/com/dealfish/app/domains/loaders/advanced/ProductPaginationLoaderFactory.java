package com.dealfish.app.domains.loaders.advanced;

import javax.inject.Inject;
import javax.inject.Singleton;

import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

import com.dealfish.app.domains.callbacks.OttoBasicCallback;
import com.squareup.otto.Bus;

@Singleton
public class ProductPaginationLoaderFactory {
	
	@Inject CallbackDFProductGateway gateway;
	@Inject OttoBasicCallback<SearchResult<Product>> callback;
	@Inject Bus bus;
	
	public SearchResultPaginationLoader<Product> loadByCategory(Integer categoryId) {
		return new SearchResultPaginationLoader<Product>(new ProductByCategoryDoRequestHandler(categoryId, gateway), callback, bus);
	}
}
