package com.dealfish.app.domains.tape;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sprint3r.deans4j.droid.catalogue.models.Product;

import com.squareup.otto.Bus;

@Singleton
public class QueueProcessor implements Callback<Product> {

	private static final int MAX_RETRIES = 5;

	@Inject
	NewPostTaskQueue queue;

	@Inject
	Bus bus;

	private boolean running;

	private int numRetries = 0;

	public void processNext() {

		if (running) {
			return; // Only one task at a time.
		}
		
		NewPostTaskItem task = queue.peek();
		if (task != null) {
			running = true;
			task.execute(this);
		}
	}

	@Override
	public void success(Product product, Response response) {
		running = false;
		queue.remove();
		bus.post(new PostNewProductSuccessEvent(product));
		
		resetRetry();
		processNext();
	}

	@Override
	public void failure(RetrofitError error) {
		numRetries++;
		running = false;
		
		if (numRetries >= MAX_RETRIES) {
			resetRetry();
			bus.post(new PostNewProductFailureEvent(numRetries));
		} else {
			processNext();
		}
	}
	
	private void resetRetry() {
		numRetries = 0;
	}
	
	public static class PostNewProductSuccessEvent {

		public final Product product;

		public PostNewProductSuccessEvent(Product product) {
			this.product = product;
		}
	}
	
	public static class PostNewProductFailureEvent {

		public final int retries;

		public PostNewProductFailureEvent(int retries) {
			this.retries = retries;
		}
	}
}
