package com.dealfish.app.droid;

import javax.inject.Inject;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.squareup.otto.Bus;

public class DFBaseService extends Service {

	@Inject
	Bus bus;

	@Override
	public void onCreate() {

		super.onCreate();
		((DFApplication) getApplication()).inject(this);
		bus.register(this);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	public Bus getBus() {
		return bus;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		bus.unregister(this);
	}

}
