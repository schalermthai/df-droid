package com.dealfish.app.droid;

import javax.inject.Inject;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.squareup.otto.Bus;

public class DFBaseFragment extends Fragment {
	
	@Inject
	Bus bus;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		((DFApplication) getActivity().getApplication()).inject(this);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		bus.register(this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		bus.unregister(this);
	}
}
