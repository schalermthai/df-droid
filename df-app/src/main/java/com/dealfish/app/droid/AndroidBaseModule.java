/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dealfish.app.droid;

import static android.content.Context.LOCATION_SERVICE;

import javax.inject.Singleton;

import android.app.Application;
import android.content.Context;
import android.location.LocationManager;

import com.squareup.otto.Bus;

import dagger.Module;
import dagger.Provides;

@Module(library = true)
public class AndroidBaseModule {
	
	private final Application application;

	public AndroidBaseModule(Application application) {
		this.application = application;
	}

	@Provides
	@Singleton
	@ForDFApplication
	public Context provideApplicationContext() {
		return application;
	}

	@Provides
	@Singleton
	public LocationManager provideLocationManager() {
		return (LocationManager) application.getSystemService(LOCATION_SERVICE);
	}

	@Provides
	@Singleton
	public Bus provideBus() {
		return new Bus();
	}
	
}
