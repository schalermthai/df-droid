package com.dealfish.app.droid;

import javax.inject.Inject;

import com.dealfish.app.R;
import com.dealfish.app.lesson3.tape.droid.PostNewProductActivity;
import com.squareup.otto.Bus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

public abstract class DFBaseActivity extends FragmentActivity {
	
	@Inject
	Bus bus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		((DFApplication) getApplication()).inject(this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		bus.register(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		bus.unregister(this);
	}
	
	public Bus getBus() {
		return bus;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_newpost:
			navigateToNewPostActiivty();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void navigateToNewPostActiivty() {
		Intent intent = new Intent(this, PostNewProductActivity.class);
		startActivity(intent);
	}
}
