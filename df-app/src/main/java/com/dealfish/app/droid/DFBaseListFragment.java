package com.dealfish.app.droid;

import javax.inject.Inject;

import com.squareup.otto.Bus;

import android.os.Bundle;
import android.support.v4.app.ListFragment;

public abstract class DFBaseListFragment extends ListFragment {
	
	@Inject
	Bus bus;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		((DFApplication) getActivity().getApplication()).inject(this);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		bus.register(this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		bus.unregister(this);
	}
	
	public Bus getBus() {
		return bus;
	}

}
