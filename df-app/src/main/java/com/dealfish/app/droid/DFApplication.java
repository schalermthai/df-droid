package com.dealfish.app.droid;

import java.util.Arrays;
import java.util.List;

import android.app.Application;

import com.dealfish.app.modules.AppModule;

import dagger.ObjectGraph;

public class DFApplication extends Application {

	private ObjectGraph graph;

	@Override
	public void onCreate() {
		super.onCreate();
		graph = ObjectGraph.create(getModules().toArray());
	}

	protected List<Object> getModules() {
		return Arrays.asList(new AndroidBaseModule(this), new AppModule());
	}

	public void inject(Object object) {
		graph.inject(object);
	}
}
