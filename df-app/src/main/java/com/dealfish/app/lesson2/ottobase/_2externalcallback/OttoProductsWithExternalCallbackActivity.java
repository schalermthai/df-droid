package com.dealfish.app.lesson2.ottobase._2externalcallback;

import javax.inject.Inject;
import javax.inject.Named;

import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.Product;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;

import com.dealfish.app.R;
import com.dealfish.app.domains.callbacks.OttoProductCache;
import com.dealfish.app.droid.DFBaseActivity;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

public class OttoProductsWithExternalCallbackActivity extends DFBaseActivity {
	
	@Inject
	CallbackDFProductGateway gateway;
	
	@Inject
	OttoProductCache callback;
	
	@Inject  @Named("productListOtto")
	Fragment listWidget;
	
	@Inject @Named("productDetailOtto")
	Fragment detailWidget;
		
	private Product selectedProductItem;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		renderProductListWidget();
		loadProducts();
	}

	private void loadProducts() {
		gateway.listProductByCategory(1, 0, 100, callback);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void renderProductListWidget() {
		
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(android.R.id.content, listWidget);
		transaction.commit();
	}
	
	public void renderProductDetailWidget() {
		
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(android.R.id.content, detailWidget);
		transaction.addToBackStack(null);
		transaction.commit();
	}
	
	@Subscribe
	public void viewSelectedProduct(Product product) {
		this.selectedProductItem = product;
		renderProductDetailWidget();
	}

	@Produce
	public Product getSelectedProductItem() {
		return selectedProductItem;
	}

}
