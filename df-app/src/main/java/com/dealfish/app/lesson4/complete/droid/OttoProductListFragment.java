package com.dealfish.app.lesson4.complete.droid;

import java.util.ArrayList;
import java.util.List;

import sprint3r.deans4j.droid.catalogue.models.Product;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.dealfish.app.domains.SomethingBadHappendEvent;
import com.dealfish.app.droid.DFBaseListFragment;
import com.squareup.otto.Subscribe;

public class OttoProductListFragment extends DFBaseListFragment {

	private ArrayAdapter<String> mAdapter;

	private Product[] data;
	
	private List<String> productNames = new ArrayList<String>();

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		mAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, productNames);

		setEmptyText("No products");
		setListAdapter(mAdapter);
		setListShown(false);
	}

	@Subscribe
	public void onListingProducts(Product[] data) {
		this.data = data;

		fillListWithData(data);

		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
	}

	private void fillListWithData(Product[] data) {
		productNames.clear();
		for (Product d : data) {
			productNames.add(d.getName());
		}
		mAdapter.notifyDataSetChanged();
	}

	@Subscribe
	public void onError(SomethingBadHappendEvent e) {
		mAdapter.clear();
		setListShown(true);
		Toast.makeText(getActivity(), "something bad happen: " + e.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		getBus().post(data[position]);
	}

}