package com.dealfish.app.lesson4.complete.droid;

import sprint3r.deans4j.droid.catalogue.models.Category;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.dealfish.app.domains.CategoryList;
import com.dealfish.app.domains.CategoryList.OnDataChangedEvent;
import com.dealfish.app.domains.SomethingBadHappendEvent;
import com.dealfish.app.droid.DFBaseListFragment;
import com.squareup.otto.Subscribe;

public class OttoCategoryListFragment extends DFBaseListFragment {
	
	private ArrayAdapter<String> mAdapter;
	
	private CategoryList categoryList;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		mAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1);

		setEmptyText("No category");
		setListAdapter(mAdapter);
		setListShown(false);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		getBus().post(categoryList.getCategories().get(position));
	}

	@Subscribe
	public void onCategoryListChanged(CategoryList categoryList) {
		if (this.categoryList == categoryList) return;
		this.categoryList = categoryList;
		this.categoryList.loadAll();
	}
	
	@Subscribe
	public void onDataChangedEvent(OnDataChangedEvent event) {
		updateCategoryList(event.categories);
		renderList();
	}

	@Subscribe
	public void onLoadingError(SomethingBadHappendEvent e) {
		renderError(e);
	}

	private void updateCategoryList(Iterable<Category> categories) {
		mAdapter.clear();
		for (Category d : categories) {
			mAdapter.add(d.getName());
		}
	}
	
	private void renderList() {
		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
	}

	private void renderError(SomethingBadHappendEvent e) {
		mAdapter.clear();
		setListShown(true);
		Toast.makeText(getActivity(), "something bad happen: " + e.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
	}

}
