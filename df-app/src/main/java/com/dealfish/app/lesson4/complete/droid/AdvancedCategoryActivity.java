package com.dealfish.app.lesson4.complete.droid;

import javax.inject.Inject;

import sprint3r.deans4j.droid.catalogue.models.Category;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.dealfish.app.domains.CategoryList;
import com.dealfish.app.droid.DFBaseActivity;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

public class AdvancedCategoryActivity extends DFBaseActivity {

	@Inject
	CategoryList categoryList;
	
	Category selectedCategoryItem;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getBus().register(categoryList);
		renderCategoryWidget();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		getBus().unregister(categoryList);
	}
	
	@Produce
	public CategoryList getCategoryList() {
		return categoryList;
	}
	
	@Subscribe
	public void onCategoryItemSelected(Category category) {
		this.selectedCategoryItem = category;
		renderProductListWidget();
	}
	
	private void renderCategoryWidget() {
		setTitle("Categories");
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(android.R.id.content, new OttoCategoryListFragment());
		transaction.commit();
	}
	
	private void renderProductListWidget() {
		Intent intent = new Intent(this, AdvancedProductActivity.class);
		intent.putExtra(AdvancedProductActivity.SELECTED_CATEGORY, selectedCategoryItem);
		startActivity(intent);
	}
	
	public Category getSelectedCategoryItem() {
		return selectedCategoryItem;
	}

}