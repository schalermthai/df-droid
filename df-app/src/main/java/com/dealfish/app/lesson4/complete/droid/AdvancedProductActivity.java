package com.dealfish.app.lesson4.complete.droid;

import javax.inject.Inject;

import sprint3r.deans4j.droid.catalogue.models.Category;
import sprint3r.deans4j.droid.catalogue.models.Product;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.Window;

import com.dealfish.app.domains.ProductList;
import com.dealfish.app.domains.ProductList.ProductListFactory;
import com.dealfish.app.domains.loaders.advanced.SearchResultPaginationLoader.OnLoadingStart;
import com.dealfish.app.droid.DFBaseActivity;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

public class AdvancedProductActivity extends DFBaseActivity {
	
	public static final String SELECTED_CATEGORY = "SELECTED_CATEGORY";

	private static final String PRODUCT_LIST_TAG = "product_list";

	private static final String PRODUCT_DETAIL_TAG = "proudct_detail";

	@Inject
	ProductListFactory productListFactory;

	ProductList productList;

	Product selectedProductItem;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
	
		Category category = (Category) getIntent().getExtras().getSerializable(SELECTED_CATEGORY);
		this.productList = productListFactory.getInstance(category);
		getBus().register(this.productList);

		renderProductListWidget();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		getBus().unregister(this.productList);
	}

	@Subscribe
	public void onProductItemSelected(SelectedProductItemEvent event) {
		this.selectedProductItem = event.product;
		if (this.selectedProductItem != null) {
			renderProductDetailWidget();
		}
	}
	
	@Subscribe
	public void onLoadingStart(OnLoadingStart event) {
		setProgressBarIndeterminateVisibility(true);
	}
	
	@Subscribe
	public void onLoadingFinish(ProductList.OnDataChangedEvent event) {
		setProgressBarIndeterminateVisibility(false);
	}
	
	@Produce
	public ProductList getProductList() {
		return productList;
	}
	
	@Produce
	public Product getSelectedProductItem() {
		return selectedProductItem;
	}

	public boolean hasMore() {
		return productList.hasMore();
	}
	
	private void renderProductListWidget() {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(android.R.id.content, new AdvancedOttoProductListFragment(), PRODUCT_LIST_TAG);
		transaction.commit();
	}
	
	private void renderProductDetailWidget() {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(android.R.id.content, new OttoProductDetailFragment(), PRODUCT_DETAIL_TAG);
		transaction.addToBackStack(null);
		transaction.commit();
	}
	
	public static class SelectedProductItemEvent {
		public final Product product;
		
		public SelectedProductItemEvent(Product product) {
			this.product = product;
		}
	}
}
