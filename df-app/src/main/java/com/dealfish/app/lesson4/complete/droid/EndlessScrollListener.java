package com.dealfish.app.lesson4.complete.droid;

import javax.inject.Inject;

import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

import com.squareup.otto.Bus;

public class EndlessScrollListener implements OnScrollListener {

    private int visibleThreshold = 2;
	private Bus bus;

	@Inject
    public EndlessScrollListener(Bus bus) {
    	this.bus = bus;
    }
    
    public EndlessScrollListener(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        if ((totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
        	bus.post(new ScrollToEndEvent());
        }
    }

	@Override
	public void onScrollStateChanged(AbsListView view, int arg1) {
		
	}
	
	public static class ScrollToEndEvent {

	}

}