package sprint3r.deans4j.droid.catalogue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rx.util.functions.Action1;
import sprint3r.deans4j.droid.catalogue.gateways.DFProductGateway;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.gateways.observable.ObservableDFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;
import sprint3r.deans4j.droid.catalogue.modules.DevEnvModule;
import dagger.Module;
import dagger.ObjectGraph;

public class ProductGatewayTest {

	@Inject
	DFProductGateway gateway;
	
	@Inject
	ObservableDFProductGateway observableGateway;
	
	@Inject
	CallbackDFProductGateway callbackGateway;
	
	private CountDownLatch lock = new CountDownLatch(1);
	
	final ResultHolder holder = new ResultHolder();

	@Before
	public void setUp() {
		ObjectGraph.create(new TestModule()).inject(this);
	}

	@Test
	public void normalGatwayHasValue() {
		Assert.assertNotNull(gateway);
		try {
			SearchResult<Product> result = gateway.listProductByCategory(1, 0, 100);
			Assert.assertNotNull(result.getItems());
			Assert.assertNotNull(result.getItems()[0].getName());
		} catch(Exception e) {
			Assert.fail("Should not happened. Maybe the Server is not up");
		}
	}
	
	@Test
	public void observableGatwayHasValue() throws InterruptedException {
		Assert.assertNotNull(gateway);

		observableGateway.listProductByCategory(1, 0, 100)
			.subscribe(new Action1<SearchResult<Product>>() {

				public void call(SearchResult<Product> result) {
					holder.result = result;
					lock.countDown();
				}
			}, new Action1<Throwable>() {

				public void call(Throwable t1) {
					lock.countDown();
				}
			});

		lock.await(2000, TimeUnit.MILLISECONDS);
		Assert.assertNotNull(holder.result);
		Assert.assertNotNull(holder.result.getItems());
		Assert.assertNotNull(holder.result.getItems()[0].getName());
	}
	
	@Test
	public void callbackGatwayHasValue() throws InterruptedException {
		Assert.assertNotNull(gateway);
		
		callbackGateway.listProductByCategory(1, 0, 100, new Callback<SearchResult<Product>>() {

			public void failure(RetrofitError error) {
				lock.countDown();
			}

			public void success(SearchResult<Product> result, Response success) {
				holder.result = result;
				lock.countDown();
			}
			
		});
		
		lock.await(2000, TimeUnit.MILLISECONDS);
		Assert.assertNotNull(holder.result);
		Assert.assertNotNull(holder.result.getItems());
		Assert.assertNotNull(holder.result.getItems()[0].getName());
	}
	
	@Module(includes = DevEnvModule.class, injects = ProductGatewayTest.class, overrides = true)
	static class TestModule {
		
	}
	
	public static class ResultHolder {
		SearchResult<Product> result;
	}

}
