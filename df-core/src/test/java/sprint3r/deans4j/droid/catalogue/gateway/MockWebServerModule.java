package sprint3r.deans4j.droid.catalogue.gateway;

import javax.inject.Singleton;

import retrofit.Server;
import sprint3r.deans4j.droid.catalogue.modules.DevEnvModule;

import com.google.mockwebserver.MockWebServer;

import dagger.Module;
import dagger.Provides;

@Module(includes = DevEnvModule.class, injects = GatewayIntegrationTest.class, overrides = true)
public class MockWebServerModule {
	
	private MockWebServer mockWebServer;

	public MockWebServerModule(MockWebServer server) {
		this.mockWebServer = server;
	}
	
	@Provides @Singleton
	public Server devEndpoint() {
		String url = mockWebServer.getUrl("/").toString();
		System.out.println(url);
		return new Server(url, "test");
	}
}
