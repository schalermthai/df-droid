package sprint3r.deans4j.droid.catalogue;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.junit.Before;
import org.junit.Test;

import retrofit.RestAdapter;
import retrofit.Server;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.JacksonConverter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.HttpResponseCache;
import com.squareup.okhttp.OkHttpClient;

public class OkHttpCacheTest {
	
	private ObjectMapper mapper;

	@Before
	public void setup() {
		mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd hh:mm"));
	}

	@Test
	public void test() throws IOException {
		OkHttpClient okHttpClient = new OkHttpClient();
	    File cacheDir = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString());
	    HttpResponseCache cache = new HttpResponseCache(cacheDir,  10L * 1024 * 1024);
	    okHttpClient.setResponseCache(cache);
	 
	    // Create a Retrofit RestAdapter for our SodaService interface.
	    Executor executor = Executors.newCachedThreadPool();
	    RestAdapter restAdapter = new RestAdapter.Builder()
	        .setExecutors(executor, executor)
	        .setClient(new OkClient(okHttpClient))
	        .setServer(new Server("http://54.213.123.110:8080/df-server"))
	        .setConverter(new JacksonConverter(mapper))
	        .build();

	    Gateway gateway = restAdapter.create(Gateway.class);
	    
	    Response response = gateway.listProductByCategory(1, 0, 10);
	    System.out.println("before cache: " + response.getHeaders());
	    
	    assertEquals(1, cache.getRequestCount());
	    assertEquals(1, cache.getNetworkCount());
	    assertEquals(0, cache.getHitCount());
	    
	    response = gateway.listProductByCategory(1, 0, 10);
	    System.out.println("after cache:  " + response.getHeaders());
	    
	    assertEquals(2, cache.getRequestCount());
	    assertEquals(1, cache.getNetworkCount());
	    assertEquals(1, cache.getHitCount());
	}
	
	public static interface Gateway {
		
		@GET("/category/{categoryId}/products")
		Response listProductByCategory(@Path("categoryId") Integer categoryId, @Query("offset") Integer offset, @Query("limit") Integer limit);
	}
}
