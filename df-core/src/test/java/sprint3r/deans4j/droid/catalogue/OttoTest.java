package sprint3r.deans4j.droid.catalogue;

import org.junit.Assert;
import org.junit.Test;

import com.squareup.otto.Bus;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;
import com.squareup.otto.ThreadEnforcer;

public class OttoTest {

	

	@Test
	public void test() {
		Bus bus = new Bus(ThreadEnforcer.ANY);
		
		Subscribers subscribers = new Subscribers();
		bus.register(subscribers);
		bus.register(new Providers());
		
		Assert.assertEquals("hello", subscribers.myevent.string);
	}

	public static class Providers {
		@Produce
		public Event produce() {
			return new Event("hello");
		}
	}
	
	public static class Subscribers {
		Event myevent;
		
		@Subscribe
		public void subscribeEvent(Event e) {
			myevent = e;
		}
	}

	public static class Event {

		public final String string;

		public Event(String string) {
			this.string = string;
		}

	}
}
