package sprint3r.deans4j.droid.catalogue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import rx.Observable;
import rx.Observer;
import rx.util.functions.Func1;
import sprint3r.deans4j.droid.catalogue.gateways.observable.ObservableDFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.gateways.observable.ObservableDFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.Category;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;
import sprint3r.deans4j.droid.catalogue.modules.DevEnvModule;
import dagger.Module;
import dagger.ObjectGraph;
import dagger.Provides;

public class ObservableRequestTest {

	@Inject
	ObservableDFProductGateway productGateway;
	
	@Inject
	ObservableDFCategoryGateway categoryGateway;

	@Inject
	Func1<Category, Observable<SearchResult<Product>>> listAllProductsInCatsFunc;
	
	@Inject
	Func1<SearchResult<Category>, Observable<Category>> convertToCategoryFunc;

	private CountDownLatch lock = new CountDownLatch(1);

	final ResultHolder holder = new ResultHolder();
	
	@Before
	public void setUp() {
		ObjectGraph.create(new TestModule()).inject(this);
	}

	@Test
	public void pararellObservableGateway() throws InterruptedException {
		
		Observable<SearchResult<Product>> cat1Products = productGateway.listProductByCategory(1, 0, 100);
		Observable<SearchResult<Product>> cat2Products = productGateway.listProductByCategory(2, 0, 100);

		Observable.merge(cat1Products, cat2Products).subscribe(collectResultsObserver());

		lock.await(10000, TimeUnit.MILLISECONDS);
		Assert.assertNotNull(holder.results);
		Assert.assertEquals(2, holder.results.size());
	}

	@Test
	public void listAllProductInAllCategories() throws InterruptedException {
		Observable<SearchResult<Category>> allCategories = categoryGateway.listAllCategories();

		allCategories.flatMap(convertToCategoryFunc)
					 .flatMap(listAllProductsInCatsFunc)
					 .subscribe(collectResultsObserver());
		

		lock.await(10000, TimeUnit.MILLISECONDS);
		Assert.assertNotNull(holder.results);
		Assert.assertEquals(8, holder.results.size());
	}

	private Observer<SearchResult<Product>> collectResultsObserver() {
		return new Observer<SearchResult<Product>>() {

			public void onCompleted() {
				lock.countDown();
			}

			public void onError(Throwable e) {
				lock.countDown();
			}

			public void onNext(SearchResult<Product> result) {
//				for (Product p : result.getItems()) {
//					System.out.println(p.getName() + " "
//							+ p.getCategoryId());
//				}
				holder.results.add(result);
			}
		};
	}
	
	public static class ResultHolder {
		List<SearchResult<Product>> results = new ArrayList<SearchResult<Product>>();
	}
	
	@Module(includes = DevEnvModule.class, injects = ObservableRequestTest.class, overrides = true)
	static class TestModule {
		
		@Provides @Singleton
		public Func1<Category, Observable<SearchResult<Product>>> provideListProductInCategoryFunc(final ObservableDFProductGateway productGateway) {
			return new Func1<Category, Observable<SearchResult<Product>>>() {
				public Observable<SearchResult<Product>> call(Category category) {
					return productGateway.listProductByCategory(category.getCategoryId(), 0, 100);
				} 
			};
		}

		@Provides @Singleton
		public Func1<SearchResult<Category>, Observable<Category>> provideConvertToCategoryFunc() {
			return new Func1<SearchResult<Category>, Observable<Category>>() {
				public Observable<Category> call(SearchResult<Category> result) {
					return Observable.from(result.getItems());
				}
			};
		}
	}
}
