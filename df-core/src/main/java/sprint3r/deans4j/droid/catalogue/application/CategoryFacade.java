package sprint3r.deans4j.droid.catalogue.application;

import java.util.List;

import sprint3r.deans4j.droid.catalogue.models.Category;

public interface CategoryFacade {
	
	List<Category> viewMainCategories();
	
	List<Category> viewAllCategories();
	
	Category viewCategoryDetail();
	
}
