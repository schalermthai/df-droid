package sprint3r.deans4j.droid.catalogue.gateways.stubs;

import java.util.ArrayList;
import java.util.List;

import sprint3r.deans4j.droid.catalogue.models.Address;
import sprint3r.deans4j.droid.catalogue.models.Category;
import sprint3r.deans4j.droid.catalogue.models.Contact;
import sprint3r.deans4j.droid.catalogue.models.DFUser;
import sprint3r.deans4j.droid.catalogue.models.Product;

public class ModelHolders {
	
	private List<Category> categories;
	private List<DFUser> users;
	private List<Product> products;

	public ModelHolders() {
		categories = createCategories("car", "home", "electronics", "fashion", "travel", "education", "lifestyle", "business");
		users = createUsers("manee", "mana", "weera", "ped");
		
		products = new ArrayList<Product>();
		
		int i = 0;
		for (Category cat : categories) {
			for (DFUser user : users) {
				for (int y = 0; y < 50; y++) {
					i++;
					products.add(createNewProduct(i, cat.getCategoryId(), user));
				}
			}
		}
	}

	public static Product createNewProduct(int id, int categoryId, DFUser user) {
		return new Product(id, "product " + id, "product description " + id, 15.0 * (id + 1), user, categoryId);
	}
	
	public List<Category> getCategories() {
		return categories;
	}
	
	public List<Product> getProducts() {
		return products;
	}
	
	public List<DFUser> getUsers() {
		return users;
	}
	
	private List<Category> createCategories(String ... categories) {
		List<Category> list = new ArrayList<Category>();

		for (int i = 0; i < categories.length; i++ ) {
			list.add(new Category(i + 1, categories[i], "category desc"));
		}
		
		return list;
	}
	
	private List<DFUser> createUsers(String ... usernames) {
		List<DFUser> list = new ArrayList<DFUser>();

		for (int i = 0; i < usernames.length; i++ ) {
			list.add(new DFUser(usernames[i], createAddress(), createContact(usernames[i])));
		}
		
		return list;
	}

	private Contact createContact(String username) {
		return new Contact(username + "@dealfish.com", "08931231234");
	}

	private Address createAddress() {
		return new Address("sukumvit road", "bangna", "bangkok", "Thailand");
	}

}
