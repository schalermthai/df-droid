package sprint3r.deans4j.droid.catalogue.application;

import java.util.List;

import sprint3r.deans4j.droid.catalogue.models.Product;

public interface HistoryFacade {
	List<Product> viewAllVisitedProducts();
}
