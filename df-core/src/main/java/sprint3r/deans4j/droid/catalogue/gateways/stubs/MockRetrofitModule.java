package sprint3r.deans4j.droid.catalogue.gateways.stubs;

import javax.inject.Singleton;

import retrofit.MockRestAdapter;
import retrofit.RestAdapter;
import sprint3r.deans4j.droid.catalogue.gateways.DFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.gateways.DFProductGateway;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.modules.RetrofitModule;
import dagger.Module;
import dagger.Provides;

@Module(library = true, includes = RetrofitModule.class, complete = false, overrides=true)
public class MockRetrofitModule {
	
	@Provides @Singleton
	public ModelHolders modelHolders() {
		return new ModelHolders();
	}
	
	@Provides @Singleton
	public MockRestAdapter mockRestAdapter(RestAdapter restAdapter) {
		return MockRestAdapter.from(restAdapter);
	}

	@Provides @Singleton
	public DFProductGateway normalProductGateway(MockRestAdapter mockRestAdapter, StubDFProductGateway stub) {
		return mockRestAdapter.create(DFProductGateway.class, stub);
	}
	
	@Provides @Singleton
	public CallbackDFProductGateway callbackProductGateway(MockRestAdapter mockRestAdapter, StubCallbackDFProductGateway stub) {
		return mockRestAdapter.create(CallbackDFProductGateway.class, stub);
	}
	
	@Provides @Singleton
	public DFCategoryGateway normalCategoryGateway(MockRestAdapter mockRestAdapter, StubDFCategoryGateway stub) {
		return mockRestAdapter.create(DFCategoryGateway.class, stub);
	}
	
	@Provides @Singleton
	public CallbackDFCategoryGateway callbackCategoryGateway(MockRestAdapter mockRestAdapter, StubCallbackDFCategoryGateway stub) {
		return mockRestAdapter.create(CallbackDFCategoryGateway.class, stub);
	}
	
}
