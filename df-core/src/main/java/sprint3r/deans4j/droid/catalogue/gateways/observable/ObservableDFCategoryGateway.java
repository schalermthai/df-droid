package sprint3r.deans4j.droid.catalogue.gateways.observable;

import retrofit.http.GET;
import rx.Observable;
import sprint3r.deans4j.droid.catalogue.models.Category;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

public interface ObservableDFCategoryGateway {
	
	@GET("/categories")
	Observable<SearchResult<Category>> listAllCategories();

}
