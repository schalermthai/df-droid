package sprint3r.deans4j.droid.catalogue.gateways;

import sprint3r.deans4j.droid.catalogue.models.DFUser;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;


public interface DFUserGateway {

	SearchResult<DFUser> findUser(String userId);
	
	SearchResult<DFUser> listFollowers(String userId, int offset, int limit);
	
	SearchResult<DFUser> listFollowing(String userId, int offset, int limit);
	
}
