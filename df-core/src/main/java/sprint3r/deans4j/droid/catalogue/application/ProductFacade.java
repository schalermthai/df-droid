package sprint3r.deans4j.droid.catalogue.application;

import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

public interface ProductFacade {
	
	SearchResult<Product> searchByCategoryId(Integer categoryId, Integer offset, Integer limit);
	
	Product viewProductById(Integer productId);
}
