package sprint3r.deans4j.droid.catalogue.gateways.stubs;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit.Callback;
import retrofit.client.Header;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.PostProduct;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

@Singleton
public class StubCallbackDFProductGateway implements CallbackDFProductGateway {
	
	@Inject
	StubDFProductGateway normalStubGateway;

	@Override
	@POST("/products")
	public void post(@Body PostProduct postProduct, Callback<Product> callback) {
		Product product = normalStubGateway.post(postProduct);
		callback.success(product, new Response(200, "OK", new ArrayList<Header>(), null));
	}

	@Override
	@GET("/product/{productId}")
	public void find(@Path("productId") Integer productId, Callback<Product> callback) {
		Product product = normalStubGateway.find(productId);
		callback.success(product, new Response(200, "OK", new ArrayList<Header>(), null));
	}

	@Override
	@GET("/category/{categoryId}/products")
	public void listProductByCategory(@Path("categoryId") Integer categoryId,
			@Query("offset") Integer offset, @Query("limit") Integer limit,
			Callback<SearchResult<Product>> callback) {
		
		SearchResult<Product> products = normalStubGateway.listProductByCategory(categoryId, offset, limit);
		callback.success(products, new Response(200, "OK", new ArrayList<Header>(), null));
	}

	@Override
	@GET("/user/{userId}/products")
	public void listProductByUser(@Path("userId") String userId,
			@Query("offset") Integer offset, @Query("limit") Integer limit,
			Callback<SearchResult<Product>> callback) {

		SearchResult<Product> products = normalStubGateway.listProductByUser(userId, offset, limit);
		callback.success(products, new Response(200, "OK", new ArrayList<Header>(), null));
	}

	@Override
	public void listFavorites(String userId, int offset, int limit,
			Callback<SearchResult<Product>> callback) {

		SearchResult<Product> products = normalStubGateway.listFavorites(userId, offset, limit);
		callback.success(products, new Response(200, "OK", new ArrayList<Header>(), null));
	}

}
