package sprint3r.deans4j.droid.catalogue.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product implements Serializable {
	
	private static final long serialVersionUID = 3711492473785956871L;
	
	Integer id;
	String name;
	String description;
	
	Date postedDate;
	Date latestUpdate;
	
	Double price;
	
	List<Property> property;
	
	List<String> images;
	
	DFUser poster;
	
	Integer categoryId;
	
	public Product() {
		
	}
	
	public Product(Integer id, String name, String description, Double price, DFUser poster, Integer categoryId) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.postedDate = new Date();
		this.latestUpdate = new Date();
		this.images = new ArrayList<String>();
		this.poster = poster;
		this.categoryId = categoryId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}

	public Date getLatestUpdate() {
		return latestUpdate;
	}

	public void setLatestUpdate(Date latestUpdate) {
		this.latestUpdate = latestUpdate;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public List<Property> getProperty() {
		return property;
	}

	public void setProperty(List<Property> property) {
		this.property = property;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImagse(List<String> images) {
		this.images = images;
	}

	public DFUser getPoster() {
		return poster;
	}

	public void setPoster(DFUser poster) {
		this.poster = poster;
	}
	
	public Integer getCategoryId() {
		return categoryId;
	}
	
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
}
