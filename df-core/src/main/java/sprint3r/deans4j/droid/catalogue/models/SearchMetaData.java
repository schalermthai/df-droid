package sprint3r.deans4j.droid.catalogue.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchMetaData implements Serializable {
	
	private static final long serialVersionUID = -5877862513499485539L;
	
	int totalSize;
	int currentOffset;
	int currentLimit;
	
	public SearchMetaData() {
		
	}
	
	public SearchMetaData(int totalSize, int currentOffset, int currentLimit) {
		this.totalSize = totalSize;
		this.currentLimit = currentLimit;
		this.currentOffset = currentOffset;
	}
	
	public int getTotalSize() {
		return totalSize;
	}
	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}
	public int getCurrentOffset() {
		return currentOffset;
	}
	public void setCurrentOffset(int currentOffset) {
		this.currentOffset = currentOffset;
	}
	public int getCurrentLimit() {
		return currentLimit;
	}
	public void setCurrentLimit(int currentLimit) {
		this.currentLimit = currentLimit;
	}
	
	public boolean hasMore() {
		return totalSize > currentOffset;
	}
	
	
}
