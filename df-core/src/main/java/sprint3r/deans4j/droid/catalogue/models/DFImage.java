package sprint3r.deans4j.droid.catalogue.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DFImage implements Serializable {

	private static final long serialVersionUID = 7619631902379214448L;
	
	String imageUrl;
	
	public String getImageUrl() {
		return imageUrl;
	}
	
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
