package sprint3r.deans4j.droid.catalogue.gateways.stubs;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit.Callback;
import retrofit.client.Header;
import retrofit.client.Response;
import retrofit.http.GET;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.models.Category;
import sprint3r.deans4j.droid.catalogue.models.SearchMetaData;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

@Singleton
public class StubCallbackDFCategoryGateway implements CallbackDFCategoryGateway {

	@Inject
	ModelHolders holders;
	
	@Override
	@GET("/categories")
	public void listAllCategories(Callback<SearchResult<Category>> callback) {
		List<Category> categories = holders.getCategories();
		SearchResult<Category> result = new SearchResult<Category>(categories.toArray(new Category[categories.size()]), new SearchMetaData(categories.size(), 0, categories.size() - 1));
		callback.success(result, new Response(200, "OK", new ArrayList<Header>(), null));
	}
	
}
