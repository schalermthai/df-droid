package sprint3r.deans4j.droid.catalogue.gateways.observable;

import rx.Observable;
import sprint3r.deans4j.droid.catalogue.models.DFUser;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;


public interface ObservableDFUserGateway {

	Observable<SearchResult<DFUser>> findUser(String userId);
	
	Observable<SearchResult<DFUser>> listFollowers(String userId, int offset, int limit);
	
	Observable<SearchResult<DFUser>> listFollowing(String userId, int offset, int limit);
	
}
