package sprint3r.deans4j.droid.catalogue.modules;

import javax.inject.Singleton;

import retrofit.RestAdapter;
import sprint3r.deans4j.droid.catalogue.gateways.DFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.gateways.DFProductGateway;
import dagger.Module;
import dagger.Provides;

@Module(library = true, includes=RetrofitModule.class, complete = false)
public class SyncGatewayModule {

	@Provides @Singleton
	public DFProductGateway normalProductGateway(RestAdapter restAdapter) {
		return restAdapter.create(DFProductGateway.class);
	}
	
	@Provides @Singleton
	public DFCategoryGateway normalCategoryGateway(RestAdapter restAdapter) {
		return restAdapter.create(DFCategoryGateway.class);
	}
}
