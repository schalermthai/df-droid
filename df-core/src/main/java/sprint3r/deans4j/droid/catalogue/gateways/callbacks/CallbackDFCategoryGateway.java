package sprint3r.deans4j.droid.catalogue.gateways.callbacks;

import retrofit.Callback;
import retrofit.http.GET;
import sprint3r.deans4j.droid.catalogue.models.Category;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

public interface CallbackDFCategoryGateway {
	
	@GET("/categories")
	void listAllCategories(Callback<SearchResult<Category>> callback);

}
