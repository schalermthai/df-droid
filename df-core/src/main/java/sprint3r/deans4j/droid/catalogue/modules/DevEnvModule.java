package sprint3r.deans4j.droid.catalogue.modules;

import java.io.File;

import javax.inject.Named;
import javax.inject.Singleton;

import retrofit.Server;

import dagger.Module;
import dagger.Provides;

@Module(includes = { SyncGatewayModule.class, ObservableGatewayModule.class, CallbackGatewayModule.class }, complete = true)
public class DevEnvModule {

	@Provides @Singleton
	public Server devEndpoint() {
		return new Server("http://54.213.123.110:8080/df-server", "test");
	}
	
	@Provides @Singleton @Named("cachedFile")
	public File cacheDir() {
		return new File(System.getProperty("java.io.tmpdir"), "okhttp-cache");
	}

}
