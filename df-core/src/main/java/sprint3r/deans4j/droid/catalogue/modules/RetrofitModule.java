package sprint3r.deans4j.droid.catalogue.modules;

import java.io.File;
import java.io.IOException;

import javax.inject.Named;
import javax.inject.Singleton;

import retrofit.RestAdapter;
import retrofit.Server;
import retrofit.client.OkClient;
import retrofit.converter.JacksonConverter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.HttpResponseCache;
import com.squareup.okhttp.OkHttpClient;

import dagger.Module;
import dagger.Provides;

@Module(library = true, includes = JacksonModule.class, complete = false)
public class RetrofitModule {
	
	@Provides @Singleton
	public OkClient okHttpClient(@Named("cachedFile") File cacheDir) {

		OkHttpClient okHttpClient = new OkHttpClient();
		
		try {
			okHttpClient.setResponseCache(new HttpResponseCache(cacheDir, 10L * 1024 * 1024));
			return new OkClient(okHttpClient);
		} catch (IOException e) {
			throw new RuntimeException("cannot create cache dir", e);
		}
	}
	
	@Provides @Singleton
	public RestAdapter restAdapter(Server endpoint, ObjectMapper mapper, OkClient okClient) {
		return new RestAdapter.Builder()
	    .setServer(endpoint)
	    .setConverter(new JacksonConverter(mapper))
	    .setClient(okClient)
	    .build();
	}
	
}
