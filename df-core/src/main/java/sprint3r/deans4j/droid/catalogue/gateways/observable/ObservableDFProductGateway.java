package sprint3r.deans4j.droid.catalogue.gateways.observable;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;
import sprint3r.deans4j.droid.catalogue.models.PostProduct;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

public interface ObservableDFProductGateway {
	
	@POST("/products")
	Observable<Product> post(@Body PostProduct product);

	@GET("/product/{productId}")
	Observable<Product> find(@Path("productId") Integer productId);
	
	@GET("/category/{categoryId}/products")
	Observable<SearchResult<Product>> listProductByCategory(@Path("categoryId") Integer categoryId, @Query("offset") Integer offset, @Query("limit") Integer limit);
	
	@GET("/user/{userId}/products")
	Observable<SearchResult<Product>> listProductByUser(@Path("userId") String userId, @Query("offset") Integer offset, @Query("limit") Integer limit);
	
	Observable<SearchResult<Product>> listFavorites(String userId, int offset, int limit);
	
}
