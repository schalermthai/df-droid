package sprint3r.deans4j.droid.catalogue.gateways.callbacks;

import retrofit.Callback;
import sprint3r.deans4j.droid.catalogue.models.DFUser;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;


public interface CallbackDFUserGateway {

	void findUser(String userId, Callback<SearchResult<DFUser>> callback);
	
	void listFollowers(String userId, int offset, int limit, Callback<SearchResult<DFUser>> callback);
	
	void listFollowing(String userId, int offset, int limit, Callback<SearchResult<DFUser>> callback);
	
}
