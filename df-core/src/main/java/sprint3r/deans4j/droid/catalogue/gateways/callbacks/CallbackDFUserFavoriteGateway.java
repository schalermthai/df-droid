package sprint3r.deans4j.droid.catalogue.gateways.callbacks;

import retrofit.Callback;
import retrofit.client.Response;

public interface CallbackDFUserFavoriteGateway {
	
	void addToFavorites(String userId, String productId, Callback<Response> callback);
	
	void removeFromFavorites(String userId, String productId, Callback<Response> callback);

}
