package sprint3r.deans4j.droid.catalogue.gateways.callbacks;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import sprint3r.deans4j.droid.catalogue.models.PostProduct;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

public interface CallbackDFProductGateway {
	
	@POST("/products")
	void post(@Body PostProduct product, Callback<Product> callback);

	@GET("/product/{productId}")
	void find(@Path("productId") Integer productId, Callback<Product> callback);
	
	@GET("/category/{categoryId}/products")
	void listProductByCategory(@Path("categoryId") Integer categoryId, @Query("offset") Integer offset, @Query("limit") Integer limit, Callback<SearchResult<Product>> callback);
	
	@GET("/user/{userId}/products")
	void listProductByUser(@Path("userId") String userId, @Query("offset") Integer offset, @Query("limit") Integer limit, Callback<SearchResult<Product>> callback);
	
	void listFavorites(String userId, int offset, int limit, Callback<SearchResult<Product>> callback);
	
}
