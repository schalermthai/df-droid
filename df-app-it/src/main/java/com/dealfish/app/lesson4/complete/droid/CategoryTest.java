package com.dealfish.app.lesson4.complete.droid;

import org.fest.assertions.api.ANDROID;

import junit.framework.Assert;
import android.annotation.SuppressLint;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.ListView;

import com.robotium.solo.Solo;
import com.squareup.spoon.Spoon;

@SuppressLint("NewApi")
public class CategoryTest extends ActivityInstrumentationTestCase2<AdvancedCategoryActivity> {

	private Solo solo;

	public CategoryTest() {
		super(AdvancedCategoryActivity.class);
	}

	public void setUp() throws Exception {
		solo = new Solo(getInstrumentation(), getActivity());
	}

	public void testPreferenceIsSaved() throws Exception {
		solo.waitForText("car");
		Spoon.screenshot(getActivity(), "category_list");

		solo.clickOnText("car");
		solo.waitForActivity(AdvancedProductActivity.class);
		solo.assertCurrentActivity("product list activity", AdvancedProductActivity.class);
		
		AdvancedProductActivity productListActivity = (AdvancedProductActivity) solo.getCurrentActivity();
		
		Spoon.screenshot(productListActivity, "cars_list");
		scrollUntilHasNoMore(productListActivity);
		
		ListView list = (ListView) solo.getView(android.R.id.list);
		
		for (int i = 0; i < list.getAdapter().getCount() && i < 10; i++) {
			
			View productItem = getProductItemView(list, i);
			navigateToProductDetail(productItem);
			String expectedProductName = "Name: product " + (i + 1);
			Spoon.screenshot(productListActivity, "product_detail");
			
			//Robotium assert
			Assert.assertTrue(solo.searchText(expectedProductName));
			
			//Fest android assert
			ANDROID.assertThat(solo.getText("Name:")).containsText(expectedProductName);
			navigateToProductList();
			
			list = (ListView) solo.getView(android.R.id.list);
		}
	}

	private void scrollUntilHasNoMore(AdvancedProductActivity activity) {
		while (activity.hasMore()) {
			solo.scrollToBottom();
			Spoon.screenshot(activity, "scroll_down");
		}
		solo.scrollToTop();
	}

	private void navigateToProductList() {
		solo.goBack();
		solo.waitForFragmentByTag("product_list");
	}

	private void navigateToProductDetail(View productItem) {
		solo.clickOnView(productItem);
		solo.waitForText("Name");
	}

	private View getProductItemView(ListView list, int i) {
		return RobotiumHelper.getViewAtIndex(list, i, getInstrumentation());
	}

	@Override
	public void tearDown() throws Exception {
		solo.finishOpenedActivities();
	}
}
