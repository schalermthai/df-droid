package com.dealfish.app.lesson4.complete.droid;

import android.app.Instrumentation;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

public class RobotiumHelper {
	
	public static View getViewAtIndex(final ListView listElement, final int indexInList, Instrumentation instrumentation) {
	    ListView parent = listElement;
	    if (parent != null) {
	        if (indexInList <= parent.getAdapter().getCount()) {
	            scrollListTo(parent, indexInList, instrumentation);
	            int indexToUse = indexInList - parent.getFirstVisiblePosition();
	            return parent.getChildAt(indexToUse);
	        }
	    }
	    return null;
	}
	
	private static <T extends AbsListView> void scrollListTo(final T listView,
	        final int index, Instrumentation instrumentation) {
	    instrumentation.runOnMainSync(new Runnable() {
	        @Override
	        public void run() {
	            listView.setSelection(index);
	        }
	    });
	    instrumentation.waitForIdleSync();
	}

}
